import sqlite3
import gspread
import pandas as pd
from oauth2client.service_account import ServiceAccountCredentials

from datetime import date
from time import sleep


def make_connection_and_add_data(date, opponent_1, opponent_2, clv):
    # define the scope
    scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']

    # add credentials to the account
    creds = ServiceAccountCredentials.from_json_keyfile_name('keys/odds-319716-794b8adca473.json', scope)

    # authorize the clientsheet
    client = gspread.authorize(creds)

    sheet = client.open('testing Betsheet - GENERAL [july21]')
    sheet_instance = sheet.get_worksheet(1)
    # count = sheet_instance.row_count
    opponents = sheet_instance.col_values(col=4)
    dates = sheet_instance.col_values(col=3)
    for i in range(len(opponents)):
        term = opponent_1 + " / " + opponent_2
        print(opponents[i], dates[i])
        if term is opponents[i] and date is dates[i]:
            print("we got a match at line:", i)


# con = sqlite3.connect('odds.db')
# cur = con.cursor()
# cur.execute('''CREATE TABLE IF NOT EXISTS events
#                (game_type text, opponent_1 text, opponent_2 text, time text, handicap_min real, handicap_max real, moneycap_min real, moneycap_max real, over real, under real, updated text)''')
# con.commit()
#
# con.commit()
# con.close()

make_connection_and_add_data("Japan", "Spain", "7/26/2021 15:00", 2)
