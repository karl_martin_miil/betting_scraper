import sqlite3
from datetime import date
from time import sleep

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

con = sqlite3.connect('odds.db')
cur = con.cursor()
cur.execute('''CREATE TABLE IF NOT EXISTS events
               (game_type text, opponent_1 text, opponent_2 text, time text, handicap_min real, handicap_max real, moneycap_min real, moneycap_max real, over real, under real, updated text)''')
con.commit()

BASKETBALL = ["https://www.pinnacle.com/en/basketball/matchups/"]
SOCCER = ["https://www.pinnacle.com/en/soccer/club-friendlies/matchups"]
BASEBALL = ["https://www.pinnacle.com/en/baseball/mlb/matchups"]

# games_to_scrape = {"BASKETBALL": BASKETBALL, "SOCCER": SOCCER, "BASEBALL": BASEBALL}
# games_to_scrape = {"SOCCER": SOCCER}
games_to_scrape = {"BASEBALL": BASEBALL}
# games_to_scrape = {"BASKETBALL": BASKETBALL, "BASEBALL": BASEBALL}

options = Options()
options.headless = True
options.add_argument('window-size=1920x1080')
options.add_argument("start-maximized")  # open Browser in maximized mode
options.add_argument("disable-infobars")  # disabling infobars
options.add_argument("--disable-extensions")  # disabling extensions
options.add_argument("--disable-gpu")  # applicable to windows os only
options.add_argument("--disable-dev-shm-usage")  # overcome limited resource problems
options.add_argument("--no-sandbox")  # Bypass OS security model
driver = webdriver.Chrome(options=options)
driver.maximize_window()


def scrape_data(game, url):
    print(game, url)
    driver.get(url)
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, 'contentBlock')))
    rows = driver.find_elements_by_class_name("style_row__3_aBC")
    for row in rows:
        opponents = row.find_elements_by_class_name("style_gameInfoLabel__2ahna")
        original_time = row.find_element_by_class_name("style_matchupDate__1gnX6")
        buttons = row.find_elements_by_class_name("style_buttons__1MJlz")
        handicaps = buttons[0].find_elements_by_class_name("style_price__15SlF")
        money_lines = buttons[1].find_elements_by_class_name("style_price__15SlF")
        ou = buttons[2].find_elements_by_class_name("style_price__15SlF")

        opponent_1 = ''
        opponent_2 = ''
        time = ''
        handicap_min = ''
        handicap_max = ''
        moneycap_min = ''
        moneycap_max = ''
        over = ''
        under = ''

        try:
            opponent_1 = opponents[0].text
        except IndexError as error:
            continue
        try:
            opponent_2 = opponents[1].text
        except IndexError as error:
            continue
        try:
            handicap_min = handicaps[0].text
        except IndexError as error:
            continue
        try:
            handicap_max = handicaps[1].text
        except IndexError as error:
            continue
        try:
            moneycap_min = money_lines[0].text
        except IndexError as error:
            continue
        try:
            moneycap_max = money_lines[1].text
        except IndexError as error:
            continue
        try:
            over = ou[0].text
        except IndexError as error:
            continue
        try:
            under = ou[1].text
        except IndexError as error:
            continue
        try:
            time = original_time.text
            time = time.replace("Today", date.today().strftime("%m/%d/%Y"))
        except IndexError as error:
            continue

        print(opponent_1, opponent_2, time, handicap_min, handicap_max, moneycap_min, moneycap_max, over, under)

        cur.execute(
            "SELECT COUNT(*) FROM events WHERE game_type=:game AND opponent_1=:op_1 AND opponent_2=:op_2 AND time=:ti",
            {"op_1": opponent_1, "op_2": opponent_2, "ti": time, "game": game})
        count = cur.fetchall()

        if count[0][0] == 0:
            cur.execute('''INSERT INTO events VALUES
                        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, datetime('now'))
                       ''', (
                game, opponent_1, opponent_2, time, handicap_min, handicap_max, moneycap_min, moneycap_max,
                over, under))
        else:
            cur.execute('''UPDATE events SET opponent_1=:op_1, opponent_2=:op_2, time=:ti, handicap_min=:h_min, handicap_max=:h_max, moneycap_min=:m_min, moneycap_max=:m_max, over=:ov, under=:un, updated=datetime('now')
                       WHERE game_type=:game AND opponent_1=:op_1 AND opponent_2=:op_2 AND time=:ti''',
                        {"op_1": opponent_1, "op_2": opponent_2, "ti": time, "h_min": handicap_min,
                         "h_max": handicap_max,
                         "m_min": moneycap_min, "m_max": moneycap_max, "ov": over, "un": under, "game": game})


for game in games_to_scrape:
    for url in games_to_scrape[game]:
        scrape_data(game, url)

con.commit()
con.close()
driver.close()
